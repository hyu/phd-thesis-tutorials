{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook\n",
    "\n",
    "import numpy as np\n",
    "import numpy.linalg as la\n",
    "import numpy.random as rd\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "rd.seed(0)\n",
    "\n",
    "from sklearn.gaussian_process import GaussianProcessRegressor\n",
    "from sklearn.gaussian_process.kernels import ConstantKernel, RBF, WhiteKernel"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Gaussian processes\n",
    "\n",
    "We define a Gaussian process by its so-called marginalization property:\n",
    "\n",
    "> A stochastic process is Gaussian if the joint probability\n",
    "distribution of any finite subset of random variables is Gaussian.\n",
    "\n",
    "The entries of the covariance matrix are determined by the covariance function of the Gaussian process, also known as its kernel.\n",
    "We introduce three particular kernels:\n",
    "\n",
    "1.  Constant kernel:\n",
    "\\begin{equation}\n",
    "k(w, w') = \\sigma_f^2 \\quad .\n",
    "\\end{equation}\n",
    "2.  White kernel:\n",
    "\\begin{equation}\n",
    "k(w, w') = \\begin{cases}\n",
    "1 & w = w' \\\\\n",
    "0 & w \\neq w'\n",
    "\\end{cases} \\quad .\n",
    "\\end{equation}\n",
    "3.  Squared-exponential (SE) kernel:\n",
    "\\begin{equation}\n",
    "k(w, w') = \\exp\\left(-\\frac{|w - w'|^2}{2 \\sigma_l^2}\\right) \\quad .\n",
    "\\end{equation}\n",
    "\n",
    "Based on their marginalization property, we have two options at our disposal to implement Gaussian processes.\n",
    "Firstly, we sample the Gaussian process at a finite resolution.\n",
    "Realizations of the countable family of joint-normally distributed random variables are found using `numpy.random`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def const_kernel(w, sigma_f=1.):\n",
    "    return sigma_f**2 * np.ones((w.size, w.size))\n",
    "\n",
    "def white_kernel(w, sigma_n=1.):\n",
    "    return sigma_n**2 * np.diag(np.ones(w.size))\n",
    "\n",
    "def se_kernel(w, sigma_l=1.):\n",
    "    U, V = np.meshgrid(w, w)\n",
    "    return np.exp(-0.5 * (U - V)**2 / sigma_l**2)\n",
    "\n",
    "w = np.linspace(0., 10., 101)\n",
    "no_realize = 100\n",
    "\n",
    "# Constant kernel.\n",
    "fig = plt.figure(1)\n",
    "ax = fig.gca()\n",
    "for i in range(no_realize):\n",
    "    ax.plot(w, rd.multivariate_normal(np.zeros_like(w), const_kernel(w)))\n",
    "ax.set_xlim((0., 10.))\n",
    "ax.set_ylim((-5., 5.))\n",
    "ax.fill_between([w[0], w[-1]], [-3., -3.], [3., 3.], alpha=0.5)\n",
    "ax.set_xlabel(\"$w$\")\n",
    "ax.set_ylabel(\"$X_w$\")\n",
    "ax.set_title(\"constant kernel ($\\sigma_f = 1$)\")\n",
    "\n",
    "# White kernel.\n",
    "fig = plt.figure(2)\n",
    "ax = fig.gca()\n",
    "for i in range(no_realize):\n",
    "    ax.plot(w, rd.multivariate_normal(np.zeros_like(w), white_kernel(w)))\n",
    "ax.set_xlim((0., 10.))\n",
    "ax.set_ylim((-5., 5.))\n",
    "ax.fill_between([w[0], w[-1]], [-3., -3.], [3., 3.], alpha=0.5)\n",
    "ax.set_xlabel(\"$w$\")\n",
    "ax.set_ylabel(\"$X_w$\")\n",
    "ax.set_title(\"white kernel ($\\Delta w = 0.1$)\")\n",
    "\n",
    "# SE kernel.\n",
    "fig = plt.figure(3)\n",
    "ax = fig.gca()\n",
    "for i in range(no_realize):\n",
    "    ax.plot(w, rd.multivariate_normal(np.zeros_like(w), se_kernel(w)))\n",
    "ax.set_xlim((0., 10.))\n",
    "ax.set_ylim((-5., 5.))\n",
    "ax.fill_between([w[0], w[-1]], [-3., -3.], [3., 3.], alpha=0.5)\n",
    "ax.set_xlabel(\"$w$\")\n",
    "ax.set_ylabel(\"$X_w$\")\n",
    "ax.set_title(\"SE kernel ($\\sigma_l = 1$)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Secondly, we resort to the implementation provided by `scikit-learn`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Constant kernel.\n",
    "fig = plt.figure(11)\n",
    "ax = fig.gca()\n",
    "Y = GaussianProcessRegressor(kernel=ConstantKernel()).sample_y(w, n_samples=no_realize)\n",
    "for i in range(no_realize):\n",
    "    ax.plot(w, Y[:, i])\n",
    "ax.set_xlim((0., 10.))\n",
    "ax.set_ylim((-5., 5.))\n",
    "ax.fill_between([w[0], w[-1]], [-3., -3.], [3., 3.], alpha=0.5)\n",
    "ax.set_xlabel(\"$w$\")\n",
    "ax.set_ylabel(\"$X_w$\")\n",
    "ax.set_title(\"constant kernel ($\\sigma_f = 1$)\")\n",
    "\n",
    "# White kernel.\n",
    "fig = plt.figure(12)\n",
    "ax = fig.gca()\n",
    "Y = GaussianProcessRegressor(kernel=WhiteKernel()).sample_y(w, n_samples=no_realize)\n",
    "for i in range(no_realize):\n",
    "    ax.plot(w, Y[:, i])\n",
    "ax.set_xlim((0., 10.))\n",
    "ax.set_ylim((-5., 5.))\n",
    "ax.fill_between([w[0], w[-1]], [-3., -3.], [3., 3.], alpha=0.5)\n",
    "ax.set_xlabel(\"$w$\")\n",
    "ax.set_ylabel(\"$X_w$\")\n",
    "ax.set_title(\"white kernel ($\\Delta w = 0.1$)\")\n",
    "\n",
    "# SE kernel.\n",
    "fig = plt.figure(13)\n",
    "ax = fig.gca()\n",
    "Y = GaussianProcessRegressor(kernel=RBF()).sample_y(w.reshape(-1, 1), n_samples=no_realize)\n",
    "for i in range(no_realize):\n",
    "    ax.plot(w, Y[:, i])\n",
    "ax.set_xlim((0., 10.))\n",
    "ax.set_ylim((-5., 5.))\n",
    "ax.fill_between([w[0], w[-1]], [-3., -3.], [3., 3.], alpha=0.5)\n",
    "ax.set_xlabel(\"$w$\")\n",
    "ax.set_ylabel(\"$X_w$\")\n",
    "ax.set_title(\"SE kernel ($\\sigma_l = 1$)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So far, so good.\n",
    "These are the Gaussian process priors.\n",
    "Note the roughness of the white kernel and the smoothness of the SE kernel.\n",
    "This is related to the smoothness of $\\kappa(\\xi) = k(w, w + \\xi)$ at $\\xi = 0$.\n",
    "For the SE kernel, $\\kappa(\\xi)$ is infinitely smooth at $\\xi = 0$.\n",
    "For the white kernel, $\\kappa(\\xi)$ is not even continuous at $\\xi = 0$.\n",
    "\n",
    "What about the Gaussian process posteriors, i.e.\\ imagine we know $X_w$ for certain $w$?\n",
    "Let us again investigate this using the two different implementations.\n",
    "Firstly, we sample the Gaussian process at a finite resolution.\n",
    "Realizations of the countable family of joint-normally distributed random variables are found using `numpy.random`.\n",
    "More precisely, they are sampled from the corresponding conditional probability distribution:\n",
    "\\begin{equation}\n",
    "X_2 \\sim \\mathcal{N}(\\mu_{2 \\mid 1}, \\Sigma_{2 \\mid 1}) \\quad , \\quad\n",
    "\\mu_{2 \\mid 1} = \\mu_2 + \\Sigma_{21} \\Sigma_{11}^{-1} \\left(x_1 - \\mu_1\\right) \\quad , \\quad\n",
    "\\Sigma_{2 \\mid 1} = \\Sigma_{22} - \\Sigma_{21} \\Sigma_{11}^{-1} \\Sigma_{12} \\quad .\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "w = np.linspace(0., 10., 101)\n",
    "no_realize = 10\n",
    "\n",
    "idx_obs = np.array([30, 70])\n",
    "y_obs = np.array([-1., 1.])\n",
    "idx_nonobs = np.array([i for i in range(len(w)) if i not in idx_obs])\n",
    "\n",
    "Sigma = se_kernel(w, sigma_l=5.)\n",
    "Sigma_11 = Sigma[idx_obs.reshape(-1, 1), idx_obs.reshape(1, -1)]\n",
    "Sigma_21 = Sigma[idx_nonobs.reshape(-1, 1), idx_obs.reshape(1, -1)]\n",
    "Sigma_22 = Sigma[idx_nonobs.reshape(-1, 1), idx_nonobs.reshape(1, -1)]\n",
    "\n",
    "mu_post_nonobs = Sigma_21 @ la.inv(Sigma_11) @ y_obs\n",
    "mu_post = np.zeros_like(w)\n",
    "mu_post[idx_obs] = y_obs\n",
    "mu_post[idx_nonobs] = mu_post_nonobs\n",
    "\n",
    "Sigma_post_nonobs = Sigma_22 - Sigma_21 @ la.inv(Sigma_11) @ Sigma_21.T\n",
    "Sigma_post = np.zeros_like(Sigma)\n",
    "Sigma_post[idx_nonobs.reshape(-1, 1), idx_nonobs.reshape(1, -1)] = Sigma_post_nonobs\n",
    "\n",
    "# numpy.random.\n",
    "fig = plt.figure(21)\n",
    "ax = fig.gca()\n",
    "for i in range(no_realize):\n",
    "    ax.plot(w, rd.multivariate_normal(mu_post, Sigma_post))\n",
    "ax.scatter(w[idx_obs], y_obs)\n",
    "ax.fill_between(w, mu_post - 3. * np.sqrt(Sigma_post[range(w.size), range(w.size)]), mu_post + 3. * np.sqrt(Sigma_post[range(w.size), range(w.size)]), color='C0', alpha=0.5)\n",
    "ax.set_xlim((0., 10.))\n",
    "ax.set_ylim((-5., 5.))\n",
    "ax.set_xlabel(\"$w$\")\n",
    "ax.set_ylabel(\"$X_w$\")\n",
    "ax.set_title(\"SE kernel ($\\sigma_f = 5$)\")\n",
    "\n",
    "# scikit-learn.\n",
    "fig = plt.figure(22)\n",
    "ax = fig.gca()\n",
    "kernel = RBF(length_scale=5., length_scale_bounds='fixed')\n",
    "gp = GaussianProcessRegressor(kernel=kernel)\n",
    "gp.fit(w[idx_obs].reshape(-1, 1), y_obs)\n",
    "y_mean, y_std = gp.predict(w.reshape(-1, 1), return_std=True)\n",
    "Y = gp.sample_y(w.reshape(-1, 1), n_samples=no_realize)\n",
    "for i in range(no_realize):\n",
    "    ax.plot(w, Y[:, i])\n",
    "ax.scatter(w[idx_obs], y_obs)\n",
    "ax.fill_between(w, y_mean - 3. * y_std, y_mean + 3. * y_std, color='C0', alpha=0.5)\n",
    "ax.set_xlim((0., 10.))\n",
    "ax.set_ylim((-5., 5.))\n",
    "ax.set_xlabel(\"$w$\")\n",
    "ax.set_ylabel(\"$X_w$\")\n",
    "ax.set_title(\"SE kernel ($\\sigma_l = 5$)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the realizations of the Gaussian process posterior interpolate between the observations with increasing variance away from the observations.\n",
    "\n",
    "In summary, Gaussian processes are no magic and definitely not black boxes.\n",
    "The learning of the hyperparameters becomes an optimization problem, and is as such not so much a machine-learning problem in principle.\n",
    "Nevertheless, we cannot be bothered to do the optimization ourselves.\n",
    "Furthermore, a lot of effort goes into making the matrix computations efficient.\n",
    "Therefore, we are going to extensively use the excellent `scikit-learn` implementation in the following examples."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Model selection example\n",
    "\n",
    "For given observations $y$, which Gaussian process model is more plausible?\n",
    "Model comparison is performed by integrating the likelihood function $P(y \\mid x, M)$ over the predictions $x$ made by the model $M$ weighted by the prior $P(x \\mid M)$, i.e. by computing the evidence.\n",
    "In the absence of noise, where the likelihood function $P(y \\mid x, M)$ becomes a delta distribution, the evidence $P(y \\mid M)$ is given by\n",
    "\\begin{align}\n",
    "P(Y = y \\mid M)\n",
    "&= \\int{P(X = x \\mid M) P(Y = y \\mid X = x, M) \\, \\mathrm{d}x} \\\\\n",
    "&= P(X = y \\mid M) \\quad .\n",
    "\\end{align}\n",
    "\n",
    "We compare two Gaussian process models with the same variance $\\sigma^2 = 1$ in the Gaussian process prior, one with a constant kernel and the other one with a white kernel.\n",
    "Qualitatively speaking, the Gaussian process with the constant kernel is the simpler model, which expresses itself in at least two ways:\n",
    "\n",
    "1.  There is a trade-off between simplicity and flexibility.\n",
    "    This means that more complex models are able to explain a wider range of observations.\n",
    "    If the observations in this example had not had the same value, then the constant kernel would not have been suitable.\n",
    "    The evidence favors simple models that agree with the given observations but not necessarily with others.\n",
    "2.  When simple models are interpolated or extrapolated from observations, there is less uncertainty.\n",
    "    Therefore, simple models are more robust when it comes to predictions.\n",
    "\n",
    "In summary, model quality from the Bayesian point of view goes hand in hand with simplicity and prediction robustness at the cost of flexibility with respect to the data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = np.linspace(-1.25, 1.25, 251)\n",
    "x_data = np.array([-1., 1.])\n",
    "y_data = np.array([0., 0.])\n",
    "\n",
    "# Constant kernel.\n",
    "kernel = ConstantKernel(constant_value=1., constant_value_bounds='fixed')\n",
    "gp = GaussianProcessRegressor(kernel=kernel)\n",
    "gp.fit(x_data.reshape(-1, 1), y_data)\n",
    "print(gp.log_marginal_likelihood())\n",
    "\n",
    "fig = plt.figure(101)\n",
    "ax = fig.gca()\n",
    "y_mean, y_std = gp.predict(x.reshape(-1, 1), return_std=True)\n",
    "ax.plot(x, y_mean, label=\"predictions\")\n",
    "ax.fill_between(x, y_mean - 3. * y_std, y_mean + 3. * y_std, color='C0', alpha=0.5)\n",
    "ax.scatter(x_data, y_data, label=\"observations\")\n",
    "ax.set_title(\"constant kernel\")\n",
    "ax.set_xlabel(\"$w$\")\n",
    "ax.set_ylabel(\"$X_w$\")\n",
    "ax.set_xlim([-1.25, 1.25])\n",
    "ax.set_ylim([-3.5, 3.5])\n",
    "ax.legend()\n",
    "\n",
    "# White kernel.\n",
    "kernel = WhiteKernel(noise_level=1., noise_level_bounds='fixed')\n",
    "gp = GaussianProcessRegressor(kernel=kernel)\n",
    "gp.fit(x_data.reshape(-1, 1), y_data)\n",
    "print(gp.log_marginal_likelihood())\n",
    "\n",
    "fig = plt.figure(102)\n",
    "ax = fig.gca()\n",
    "y_mean, y_std = gp.predict(x.reshape(-1, 1), return_std=True)\n",
    "ax.plot(x, y_mean, label=\"predictions\")\n",
    "ax.fill_between(x, y_mean - 3. * y_std, y_mean + 3. * y_std, color='C0', alpha=0.5)\n",
    "ax.scatter(x_data, y_data, label=\"observations\")\n",
    "ax.set_title(\"white kernel\")\n",
    "ax.set_xlabel(\"$w$\")\n",
    "ax.set_ylabel(\"$X_w$\")\n",
    "ax.set_xlim([-1.25, 1.25])\n",
    "ax.set_ylim([-3.5, 3.5])\n",
    "ax.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Another model selection example\n",
    "\n",
    "We are given a series of noise-free observations $y$ over the interval $-1 \\leq w \\leq 1$:\n",
    "\\begin{equation}\n",
    "y = w^6 \\quad .\n",
    "\\end{equation}\n",
    "\n",
    "In order to explain the observations, we have two models $M_1$ and $M_2$ at our disposal.\n",
    "The model equations are respectively given by\n",
    "\\begin{equation}\n",
    "x_1 = a w^2 \\quad ,\n",
    "\\end{equation}\n",
    "\\begin{equation}\n",
    "x_2 = a w^4 \\quad ,\n",
    "\\end{equation}\n",
    "where the parameter $a$ is to be determined.\n",
    "We have prior knowledge that the parameter $a$ has unit order of magnitude:\n",
    "\\begin{equation}\n",
    "a \\sim \\mathcal{N}(0, 1) \\quad .\n",
    "\\end{equation}\n",
    "\n",
    "In practice, we do not know the true functional relationship between w and y.\n",
    "Hence, we pretend here to be oblivious of the truth, too.\n",
    "Therefore, we are now going to answer the following question:\n",
    "For given observations $y$, which model is more plausible?\n",
    "\n",
    "Note that the observations do not include any noise.\n",
    "Hence, we are learning parameters such that the model predictions exactly fit the data.\n",
    "Unless a model is perfect, it is not possible to achieve this with constant parameters.\n",
    "For every observation $y$, we are able to calculate a corresponding value for the parameter $a$ because the observations are scalar and there is exactly one parameter.\n",
    "In practice, this is not possible when the number of parameters exceeds the number of degrees of freedom of each observation.\n",
    "This is the subject of nonparametric regression, which we are going to discuss in another notebook.\n",
    "In this special case, it is possible to have $a$ as a function of $w$ taken from the Gaussian process posterior."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x_data = np.linspace(-1., 1., 5)\n",
    "\n",
    "# First model.\n",
    "y_data = x_data**4\n",
    "kernel = ConstantKernel(1.) * RBF(0.5)\n",
    "gp = GaussianProcessRegressor(kernel=kernel)\n",
    "gp.fit(x_data.reshape(-1, 1), y_data)\n",
    "print(gp.log_marginal_likelihood())\n",
    "print(gp.kernel_)\n",
    "\n",
    "fig = plt.figure(201)\n",
    "ax = fig.gca()\n",
    "y_mean, y_std = gp.predict(x.reshape(-1, 1), return_std=True)\n",
    "ax.plot(x, y_mean, label=\"predictions\")\n",
    "ax.fill_between(x, y_mean - 3. * y_std, y_mean + 3. * y_std, color='C0', alpha=0.5)\n",
    "ax.scatter(x_data, y_data, label=\"observations\")\n",
    "ax.set_title(\"$x = a w^2$\")\n",
    "ax.set_xlabel(\"$w$\")\n",
    "ax.set_ylabel(\"$a$\")\n",
    "ax.set_xlim([-1.25, 1.25])\n",
    "ax.set_ylim([-1., 3.])\n",
    "ax.legend()\n",
    "\n",
    "# Second model.\n",
    "y_data = x_data**2\n",
    "kernel = ConstantKernel(1.) * RBF(0.5)\n",
    "gp = GaussianProcessRegressor(kernel=kernel)\n",
    "gp.fit(x_data.reshape(-1, 1), y_data)\n",
    "print(gp.log_marginal_likelihood())\n",
    "print(gp.kernel_)\n",
    "\n",
    "fig = plt.figure(202)\n",
    "ax = fig.gca()\n",
    "y_mean, y_std = gp.predict(x.reshape(-1, 1), return_std=True)\n",
    "ax.plot(x, y_mean, label=\"predictions\")\n",
    "ax.fill_between(x, y_mean - 3. * y_std, y_mean + 3. * y_std, color='C0', alpha=0.5)\n",
    "ax.scatter(x_data, y_data, label=\"observations\")\n",
    "ax.set_title(\"$x = a w^4$\")\n",
    "ax.set_xlabel(\"$w$\")\n",
    "ax.set_ylabel(\"$a$\")\n",
    "ax.set_xlim([-1.25, 1.25])\n",
    "ax.set_ylim([-1., 3.])\n",
    "ax.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As expected, the model $M_2$ with $x_2 = a x^4$ fits the observations from $y = x^6$ better than the model $M_1$ with $x_1 = a x^2$.\n",
    "There are three ways to come to this conclusion:\n",
    "\n",
    "1.  The exponent of $M_2$ ($n = 4$) is closer to the value of $n = 6$ than the exponent of $M_1$ ($n = 2$).\n",
    "    Obviously, this is not generalizable because we do not know that the observations follow $y = w^6$, or any polynomial for that matter, in reality.\n",
    "2.  We perform model comparison using the evidence or, equivalently, the log-marginal likelihood.\n",
    "    The larger the log-marginal likelihood, the more plausible the model (`-4.13` vs `-4.77`).\n",
    "3.  It is also insightful to examine the length scale $\\sigma_l$ of the SE kernel.\n",
    "    Larger $\\sigma_l$ indicates less variation of $a$ with $w$, which makes for a more robust and plausible model (`0.933` vs `0.291`).\n",
    "\n",
    "Another question:\n",
    "Why do we bother coming up with models such as $M_1$ of $M_2$ in the first place?\n",
    "Instead of finding the Gaussian process posterior for a parameter $a$, why do we not use Gaussian processes to interpolate $y$ directly?\n",
    "\n",
    "My answer:\n",
    "In general, a natural phenomenon worthwhile modeling involves observations $y$ that significantly vary with $w$, which represents a design parameter of the experiment.\n",
    "The modeler hopes to find a reduced-order model with a small number of parameters that explains the experiment over a wide range of design parameters.\n",
    "In the ideal case, the parameters are perfectly constant.\n",
    "However, it is already an improvement if the parameters are found to be nearly constant within a certain regime.\n",
    "\n",
    "In summary, the goal of reduced-order modeling is to shift the perspective from the state space, which we are able to (partially) observe, to the parameter space.\n",
    "In general, the parameters are hidden from the modeler, and as such at best inferrable but categorically unobservable.\n",
    "The advantage we stand to gain from shifting the perspective to the parameter space is more smoothness and thus more robust predictions based on the theory of stochastic processes.\n",
    "From a Bayesian point of view, this is reflected by the value of the evidence of the model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_data = x_data**6\n",
    "kernel = ConstantKernel(1.) * RBF(0.2)\n",
    "gp = GaussianProcessRegressor(kernel=kernel)\n",
    "gp.fit(x_data.reshape(-1, 1), y_data)\n",
    "print(gp.log_marginal_likelihood())\n",
    "print(gp.kernel_)\n",
    "\n",
    "fig = plt.figure(211)\n",
    "ax = fig.gca()\n",
    "y_mean, y_std = gp.predict(x.reshape(-1, 1), return_std=True)\n",
    "ax.plot(x, y_mean, label=\"predictions\")\n",
    "ax.fill_between(x, y_mean - 3. * y_std, y_mean + 3. * y_std, color='C0', alpha=0.5)\n",
    "ax.scatter(x_data, y_data, label=\"observations\")\n",
    "ax.set_title(\"$y = x$\")\n",
    "ax.set_xlabel(\"$w$\")\n",
    "ax.set_ylabel(\"$x$\")\n",
    "ax.set_xlim([-1.25, 1.25])\n",
    "ax.set_ylim([-1., 3.])\n",
    "ax.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Just for fun:\n",
    "What happens when the model perfectly represents the observations, i.e. $x = a w^6$?\n",
    "For each observation $y$, we estimate $a = 1$.\n",
    "The optimized length scale is `1e+05`, and would be infinite if it were not for length scale bounds in the `scikit-learn` implementation.\n",
    "In fact, the log-marginal likelihood is not `39.53` but the value $-\\frac{1}{2} - \\frac{\\log(2 \\pi)}{2} \\approx -1.42$ of the corresponding constant kernel.\n",
    "This value of the evidence is still significantly higher than for $M_1$ and $M_2$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_data = np.ones_like(x_data)\n",
    "kernel = RBF(0.2)\n",
    "gp = GaussianProcessRegressor(kernel=kernel)\n",
    "gp.fit(x_data.reshape(-1, 1), y_data)\n",
    "print(gp.log_marginal_likelihood())\n",
    "print(gp.kernel_)\n",
    "\n",
    "fig = plt.figure(221)\n",
    "ax = fig.gca()\n",
    "y_mean, y_std = gp.predict(x.reshape(-1, 1), return_std=True)\n",
    "ax.plot(x, y_mean, label=\"predictions\")\n",
    "ax.fill_between(x, y_mean - 3. * y_std, y_mean + 3. * y_std, color='C0', alpha=0.5)\n",
    "ax.scatter(x_data, y_data, label=\"observations\")\n",
    "ax.set_title(\"$x = a w^6$\")\n",
    "ax.set_xlabel(\"$w$\")\n",
    "ax.set_ylabel(\"$x$\")\n",
    "ax.set_xlim([-1.25, 1.25])\n",
    "ax.set_ylim([-1., 3.])\n",
    "ax.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Wiener process\n",
    "\n",
    "We define a Wiener process by its so-called Markov property:\n",
    "\\begin{equation}\n",
    "P(X_t \\mid X_s) = P(X_{t-s}) \\quad , \\quad 0 < s < t \\quad .\n",
    "\\end{equation}\n",
    "More specifically, the Wiener process follows a normal distribution:\n",
    "\\begin{equation}\n",
    "P(X_t) = \\mathcal{N}(0, t) \\quad ,\n",
    "\\end{equation}\n",
    "with zero mean and a standard deviation proportional to $\\sqrt{t}$.\n",
    "\n",
    "Realizations of the Wiener process are also called Brownian motion.\n",
    "The underlying physical picture behind this name is the undirected motion of a small particle, e.g. pollen, immersed in a liquid.\n",
    "The molecular dynamics of the liquid cause liquid molecules to bump into the particle.\n",
    "In the long term, the net impact on the particle is zero.\n",
    "In the short term, there are streaks of liquid molecules hitting the particle one-sidedly, causing the particle to drift randomly.\n",
    "\n",
    "We illustrate this idea with a simple script that compares three things:\n",
    "\n",
    "1.  The Wiener process $(X_t)_{t \\in [0, 1]}$ with $X_t \\sim \\mathcal{N}(0, t)$.\n",
    "2.  Brownian motion, i.e. realizations $x(t)$ sampled from the Wiener process satisfying $P(X_t \\mid X_s) = P(X_{t-s})$.\n",
    "The approximation of the statistics of the Wiener process by Brownian motion is improved by increasing the number of realizations `N_ens`.\n",
    "3.  Random walks, i.e. discretizations of Brownian motion at different resolutions $\\Delta t$.\n",
    "The fractal nature of Brownian motion is retrieved by increasing the resolution `N` of the random walks.\n",
    "\n",
    "We invite the reader to play around with these settings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def brown(T, N, N_ens=1):\n",
    "    dt = T / N\n",
    "    sqrt_dt = np.sqrt(dt)\n",
    "\n",
    "    dW = rd.normal(0., sqrt_dt, (N_ens, N))\n",
    "    W = np.zeros((N_ens, N + 1))\n",
    "    W[:, 1:] = np.cumsum(dW, axis=1)\n",
    "\n",
    "    return W, dW\n",
    "\n",
    "T = 1.\n",
    "N = 1000\n",
    "t = np.linspace(0., 1., N + 1)\n",
    "\n",
    "N_ens = 10000\n",
    "W_ens, dW_ens = brown(T, N, N_ens)\n",
    "\n",
    "fig = plt.figure(301)\n",
    "ax = fig.gca()\n",
    "for k in range(100):\n",
    "    ax.plot(t, W_ens[k, :])\n",
    "ax.fill_between(t, -3. * np.sqrt(t), 3. * np.sqrt(t), color='C0', alpha=0.5)\n",
    "ax.set_xlabel(r\"$w$\")\n",
    "ax.set_ylabel(r\"$X_w$\")\n",
    "ax.set_xlim([0., 1.])\n",
    "ax.set_ylim([-3.5, 3.5])\n",
    "    \n",
    "fig = plt.figure(302)\n",
    "ax = fig.gca()\n",
    "std_dev = np.sqrt(np.sum(W_ens**2, axis=0) / N_ens)\n",
    "ax.plot(t, np.sqrt(t), label=\"analytical\")\n",
    "ax.plot(t, std_dev, label=\"numerical\")\n",
    "ax.set_xlabel(r\"$w$\")\n",
    "ax.set_ylabel(r\"standard deviation of $X_w$\")\n",
    "ax.set_xlim([0., 1.])\n",
    "ax.set_ylim([0., 1.])\n",
    "ax.legend()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
