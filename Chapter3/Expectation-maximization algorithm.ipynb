{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook\n",
    "\n",
    "import numpy as np\n",
    "import numpy.linalg as la\n",
    "import numpy.random as rd\n",
    "from scipy.stats import multivariate_normal\n",
    "from matplotlib.animation import FuncAnimation\n",
    "from matplotlib.patches import Circle\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "rd.seed(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We provide a hands-on demonstration of the expectation-maximization algorithm without the mathematical detail.\n",
    "The example we pick is clustering, an unsupervised learning task.\n",
    "We pick this example for a variety of reasons:\n",
    "\n",
    "*   It is very visual and intuitive.\n",
    "    You will immediately get it unless you are a machine.\n",
    "*   On this note, it raises an important point:\n",
    "    Clustering, as you will see, is easy for humans but very difficult for machines.\n",
    "    I.e., it is not straightforward to translate clustering into an algorithm that a machine is able to execute.\n",
    "*   On another note, I find this example to be a lot of fun, even stand-alone.\n",
    "    Every time, it fascinates me that the algorithm works (viz. converges).\n",
    "    It really works surprisingly well.\n",
    "\n",
    "At first sight, it may not be evident how all this is relevant to reduced-order modeling.\n",
    "Nevertheless, we will come back to this at the very end."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Algorithm\n",
    "\n",
    "The expectation-maximization algorithm is a sequential or iterative version of the evidence framework.\n",
    "In the evidence framework, we distinguish between two types of parameters:\n",
    "\n",
    "1.  **Model parameters** are treated as so-called _latent variables_ (also known as _hidden variables_).\n",
    "    Latent variables provide physically meaningful information about a data point but impossible to directly observe.\n",
    "    The objective for latent variables is statistical inference.\n",
    "    The objective is not a single point estimate but a probability distribution (viz. degrees of belief) over a range of possible values.\n",
    "2.  **Hyperparameters** are so-called _nuisance parameters_.\n",
    "    They are not associated with any individual data point.\n",
    "    The objective for hyperparameters is optimization.\n",
    "    Hyperparameters are only means to an end, and we are not truly interested in their values or uncertainties.\n",
    "    That is why they are called nuisance parameters.\n",
    "\n",
    "After the initialization of the hyperparameters, e.g. with random values, the expectation-maximization step alternates between the following two steps:\n",
    "\n",
    "1.  In the expectation step, we perform statistical inference over the latent variables, using the nuisance parameters from the previous step.\n",
    "2.  In the maximization step, we optimize the nuisance parameters to fit the latent variables from the expectation step.\n",
    "\n",
    "We are now going to present an example of clustering, and propose a number of successively more sophisticated versions of the expectation-maximization algorithm.\n",
    "This example is based on Chapters 20 and 22 from the excellent David MacKay's book ['Information theory, inference, and learning algorithms'](http://www.inference.phy.cam.ac.uk/mackay/itila/) (Cambridge University Press, 2003)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Example\n",
    "\n",
    "There are two clusters:\n",
    "\n",
    "1.  The first cluster is centered around $m_1 = (0, 0)$, and sampled from a normal distribution of radius $\\sigma_1 = 0.5$.\n",
    "2.  The second cluster is centered around $m_2 = (2, 1)$, and sampled from a normal distribution of radius $\\sigma_2 = 0.3$.\n",
    "\n",
    "Each data point is generated as follows:\n",
    "\n",
    "1.  Pick cluster 1 at a probability of $\\pi_1 = 2 / 3$ or cluster 2 at a probability of $\\pi_2 = 1 / 3$.\n",
    "2.  Sample point from picked cluster according to normal distributions outlined above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Hyperparameters and prior probabilities.\n",
    "m_1 = np.array([0., 0.])\n",
    "m_2 = np.array([2., 1.])\n",
    "Sigma_1 = np.diag(0.5**2 * np.ones(2))\n",
    "Sigma_2 = np.diag(0.3**2 * np.ones(2))\n",
    "pi_1 = 2. / 3.\n",
    "pi_2 = 1. / 3.\n",
    "\n",
    "# Sample.\n",
    "no_points = 1000\n",
    "sol = rd.choice(2, no_points, p=[pi_1, pi_2])\n",
    "x = (1. - sol).reshape(-1, 1) * rd.multivariate_normal(m_1, Sigma_1, size=no_points) + sol.reshape(-1, 1) * rd.multivariate_normal(m_2, Sigma_2, size=no_points)\n",
    "x_1 = x[sol == 0, :]\n",
    "x_2 = x[sol == 1, :]\n",
    "\n",
    "# Visualize.\n",
    "fig = plt.figure(1)\n",
    "ax = fig.gca()\n",
    "ax.scatter(x_1[:, 0], x_1[:, 1])\n",
    "ax.scatter(x_2[:, 0], x_2[:, 1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Imagine now that we do not know anything about how the data points have been generated.\n",
    "We are given the data points, and we are told that there were two clusters in the first place -- nothing else.\n",
    "The clustering problem is to tell whether a data point belongs to cluster 1 or 2.\n",
    "\n",
    "We compare the clustering problem to two other types of problem that a physically inclined reader is possibly more familiar with:\n",
    "\n",
    "1.  In a regression problem, the model parameters are usually continuous.\n",
    "    In our clustering problem, the quantity of interest is a categorical variable, and has the value 0 for cluster 1 and 1 for cluster 2, for example.\n",
    "    As in Bayesian regression, it is also possible to assign probabilities to the values of the categorical variable, which makes the clustering problem continuous again in a certain sense.\n",
    "2.  In a classification problem, the classes are well-defined.\n",
    "    In our clustering problem, we do not know the centers or anything else of the clusters beforehand.\n",
    "    Therefore, we must learn both the membership of the data points and the definition of the clusters.\n",
    "    Note that this is an unsupervised problem.\n",
    "    In a supervised classification problem, there is training data where the membership of each data point is given.\n",
    "    From the training data, it is possible to learn the definition of the classes, and use that for the testing data.\n",
    "    In the unsupervised clustering problem, there are no a-priori observations, and hence no distinction between training and testing data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Blank slate.\n",
    "fig = plt.figure(2)\n",
    "ax = fig.gca()\n",
    "ax.scatter(x[:, 0], x[:, 1], c='grey')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Hard K-means clustering\n",
    "\n",
    "The hard K-means clustering algorithm uses the following nuisance parameters:\n",
    "\n",
    "*   Centers $m_1$ and $m_2$.\n",
    "\n",
    "The expectation-maximization algorithm is as follows:\n",
    "\n",
    "1.  In the expectation step, the Eucliden distances to the centers from the previous step are compared.\n",
    "    If a data point is closer to the first center, it belongs to cluster 1.\n",
    "    If a data point is closer to the second center, it belongs to cluster 2.\n",
    "2.  In the maximization step, the centers of the clusters are calculated by averaging the coordinates of the respective data points as identified in the expectation step.\n",
    "\n",
    "This algorithm is called 'hard' because every data point is an exclusive member of exactly one cluster."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Euclidean distance.\n",
    "def dist(m, x):\n",
    "    return la.norm(x - m, axis=-1)\n",
    "\n",
    "# Hyperparameters.\n",
    "no_em = 10\n",
    "r_list = np.zeros((no_points, no_em), dtype='bool')\n",
    "m_1_list = np.zeros((2, no_em + 1))\n",
    "m_2_list = np.zeros((2, no_em + 1))\n",
    "\n",
    "# Initialize (randomly).\n",
    "m_1_list[:, 0] = [-1., -1.]\n",
    "m_2_list[:, 0] = [-1., 1.]\n",
    "\n",
    "for i in range(no_em):\n",
    "    # Expectation step.\n",
    "    r_list[:, i] = (dist(m_1_list[:, i], x) > dist(m_2_list[:, i], x))\n",
    "    res_1 = x[np.logical_not(r_list[:, i]), :]\n",
    "    res_2 = x[r_list[:, i], :]\n",
    "    \n",
    "    # Maximization step.\n",
    "    m_1_list[:, i + 1] = np.mean(res_1, axis=0)\n",
    "    m_2_list[:, i + 1] = np.mean(res_2, axis=0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Visualize.\n",
    "fig = plt.figure(21)\n",
    "ax = fig.gca()\n",
    "paths_m = ax.scatter([m_1_list[0, 0], m_2_list[0, 0]], [m_1_list[1, 0], m_2_list[1, 0]], marker='x', c='k', zorder=2)\n",
    "paths_x = ax.scatter(x[:, 0], x[:, 1], c='grey', alpha=0.5)\n",
    "\n",
    "# Animate.\n",
    "def update(frame):\n",
    "    ax.set_title(\"iteration {}\".format(frame + 1))\n",
    "    paths_m.set_offsets([m_1_list[:, frame + 1], m_2_list[:, frame + 1]])\n",
    "    paths_x.set_facecolors([\"C0\" if not r_list[i, frame] else \"C1\" for i in range(no_points)])\n",
    "\n",
    "ani = FuncAnimation(fig, update, frames=range(no_em), interval=1000, repeat=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the classification of the data points somewhere in the middle are most challenging."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Optimized hyperparameters.\n",
    "print(m_1_list[:, -1])\n",
    "print(m_2_list[:, -1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Soft K-means clustering (v1)\n",
    "\n",
    "Our first version of the soft K-means clustering algorithm uses the following nuisance parameters:\n",
    "\n",
    "*   Centers $m_1$ and $m_2$.\n",
    "*   Radii $\\sigma_1$ and $\\sigma_2$.\n",
    "\n",
    "The expectation-maximization algorithm is as follows:\n",
    "\n",
    "1.  In the expectation step, the likelihoods with means and standard deviations based on the centers and the radii from the previous step are compared.\n",
    "    A data point belongs to the cluster more likely.\n",
    "    Nevertheless, we define for each data point a responsibility $r_k$ of the cluster $k$.\n",
    "    The responsibility $r_k$ for each data point gives the probability that the data point belongs to the cluster $k$.\n",
    "    The responsibility is the continuous version of the binary membership 0 or 1.\n",
    "2.  In the maximization step, the centers of the clusters are calculated by averaging the coordinates of the data points weighted by the respective responsibilities.\n",
    "    The variances, i.e. the squares of the standard deviations, are similarly calculated.\n",
    "\n",
    "This algorithm is called 'soft' because the responsibilities generally assume values between 0 and 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Probability of normal distribution.\n",
    "def prob(m, x, sigma=1.):\n",
    "    return multivariate_normal.pdf(x, m, np.diag(sigma**2 * np.ones(2)))\n",
    "\n",
    "# Hyperparameters.\n",
    "no_em = 15\n",
    "r_list = np.zeros((no_points, no_em))\n",
    "m_1_list = np.zeros((2, no_em + 1))\n",
    "m_2_list = np.zeros((2, no_em + 1))\n",
    "sigma_1_list = np.zeros(no_em + 1)\n",
    "sigma_2_list = np.zeros(no_em + 1)\n",
    "\n",
    "# Initialize (randomly).\n",
    "m_1_list[:, 0] = [-1., -1.]\n",
    "m_2_list[:, 0] = [-1., 1.]\n",
    "sigma_1_list[0] = 1.\n",
    "sigma_2_list[0] = 1.\n",
    "\n",
    "for i in range(no_em):\n",
    "    # Expectation step.\n",
    "    r_list[:, i] = prob(m_1_list[:, i], x, sigma_1_list[i]) / (prob(m_1_list[:, i], x, sigma_1_list[i]) + prob(m_2_list[:, i], x, sigma_2_list[i]))\n",
    "    \n",
    "    # Maximization step.\n",
    "    m_1_list[:, i + 1] = np.sum(r_list[:, i].reshape(-1, 1) * x, axis=0) / np.sum(r_list[:, i])\n",
    "    m_2_list[:, i + 1] = np.sum((1. - r_list[:, i]).reshape(-1, 1) * x, axis=0) / (no_points - np.sum(r_list[:, i]))\n",
    "    sigma_1_list[i + 1] = np.sqrt(0.5 * np.sum(r_list[:, i] * dist(x, m_1_list[:, i])**2, axis=0) / np.sum(r_list[:, i]))\n",
    "    sigma_2_list[i + 1] = np.sqrt(0.5 * np.sum((1. - r_list[:, i]) * dist(x, m_2_list[:, i])**2, axis=0) / (no_points - np.sum(r_list[:, i])))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Visualize.\n",
    "fig = plt.figure(41)\n",
    "ax = fig.gca()\n",
    "paths_m = ax.scatter([m_1_list[0, 0], m_2_list[0, 0]], [m_1_list[1, 0], m_2_list[1, 0]], marker='x', c='k', zorder=2)\n",
    "paths_x = ax.scatter(x[:, 0], x[:, 1], c='grey', alpha=0.5)\n",
    "circle_1 = Circle(m_1_list[:, 0], 3. * sigma_1_list[0], color='C0', alpha=0.5)\n",
    "ax.add_artist(circle_1)\n",
    "circle_2 = Circle(m_2_list[:, 0], 3. * sigma_2_list[0], color='C1', alpha=0.5)\n",
    "ax.add_artist(circle_2)\n",
    "\n",
    "# Animate.\n",
    "def update(frame):\n",
    "    ax.set_title(\"iteration {}\".format(frame + 1))\n",
    "    paths_m.set_offsets([m_1_list[:, frame + 1], m_2_list[:, frame + 1]])\n",
    "    paths_x.set_facecolors([\"C0\" if r_list[i, frame] > 0.5 else \"C1\" for i in range(no_points)])\n",
    "    circle_1.set_center(m_1_list[:, frame + 1])\n",
    "    circle_2.set_center(m_2_list[:, frame + 1])\n",
    "    circle_1.set_radius(3. * sigma_1_list[frame + 1])\n",
    "    circle_2.set_radius(3. * sigma_2_list[frame + 1])\n",
    "\n",
    "ani = FuncAnimation(fig, update, frames=range(no_em), interval=1000, repeat=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the classification of the data points somewhere in the middle is now more satisfactory.\n",
    "Nevertheless, look at the two orange points outside both 3-$\\sigma$ confidence regions near the intersection of the two confidence regions towards the lower right.\n",
    "In Figure 1, at least one of them is blue simply because cluster 1 has the higher prior probability."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Optimized hyperparameters.\n",
    "print(m_1_list[:, -1])\n",
    "print(m_2_list[:, -1])\n",
    "print(sigma_1_list[-1])\n",
    "print(sigma_2_list[-1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Soft K-means clustering with priors (v2)\n",
    "\n",
    "Our second version of the soft K-means clustering algorithm uses the following nuisance parameters:\n",
    "\n",
    "*   Centers $m_1$ and $m_2$.\n",
    "*   Radii $\\sigma_1$ and $\\sigma_2$.\n",
    "*   Prior probabilities $\\pi_1$ and $\\pi_2$.\n",
    "\n",
    "The expectation-maximization algorithm is as follows:\n",
    "\n",
    "1.  In the expectation step, the posterior probabilities with means and standard deviations for the likelihood function based on the centers and the radii from the previous step as well as the prior probabilities from the previous step are compared.\n",
    "    A data point belongs to the cluster more probable.\n",
    "    We calculate for each data point a responsibility $r_k$ of the cluster $k$.\n",
    "2.  In the maximization step, the centers of the clusters are calculated by averaging the coordinates of the data points weighted by the respective responsibilities.\n",
    "    The variances, i.e. the squares of the standard deviations, are similarly calculated.\n",
    "    The prior probabilities are calculated via 'soft' counting, i.e. summation over the responsibilities."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Hyperparameters and prior probabilities.\n",
    "no_em = 15\n",
    "r_list = np.zeros((no_points, no_em))\n",
    "m_1_list = np.zeros((2, no_em + 1))\n",
    "m_2_list = np.zeros((2, no_em + 1))\n",
    "sigma_1_list = np.zeros(no_em + 1)\n",
    "sigma_2_list = np.zeros(no_em + 1)\n",
    "pi_1_list = np.zeros(no_em + 1)\n",
    "pi_2_list = np.zeros(no_em + 1)\n",
    "\n",
    "# Initialize (randomly).\n",
    "m_1_list[:, 0] = [-1., -1.]\n",
    "m_2_list[:, 0] = [-1., 1.]\n",
    "sigma_1_list[0] = 1.\n",
    "sigma_2_list[0] = 1.\n",
    "pi_1_list[0] = 0.5\n",
    "pi_2_list[0] = 0.5\n",
    "\n",
    "for i in range(no_em):\n",
    "    # Expectation step.\n",
    "    r_list[:, i] = pi_1_list[i] * prob(m_1_list[:, i], x, sigma_1_list[i]) / (pi_1_list[i] * prob(m_1_list[:, i], x, sigma_1_list[i]) + pi_2_list[i] * prob(m_2_list[:, i], x, sigma_2_list[i]))\n",
    "    \n",
    "    # Maximization step.\n",
    "    m_1_list[:, i + 1] = np.sum(r_list[:, i].reshape(-1, 1) * x, axis=0) / np.sum(r_list[:, i])\n",
    "    m_2_list[:, i + 1] = np.sum((1. - r_list[:, i]).reshape(-1, 1) * x, axis=0) / (no_points - np.sum(r_list[:, i]))\n",
    "    sigma_1_list[i + 1] = np.sqrt(0.5 * np.sum(r_list[:, i] * dist(x, m_1_list[:, i])**2, axis=0) / np.sum(r_list[:, i]))\n",
    "    sigma_2_list[i + 1] = np.sqrt(0.5 * np.sum((1. - r_list[:, i]) * dist(x, m_2_list[:, i])**2, axis=0) / (no_points - np.sum(r_list[:, i])))\n",
    "    pi_1_list[i + 1] = np.sum(r_list[:, i]) / no_points\n",
    "    pi_2_list[i + 1] = (no_points - np.sum(r_list[:, i])) / no_points"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Visualize.\n",
    "fig = plt.figure(51)\n",
    "ax = fig.gca()\n",
    "paths_m = ax.scatter([m_1_list[0, 0], m_2_list[0, 0]], [m_1_list[1, 0], m_2_list[1, 0]], marker='x', c='k', zorder=2)\n",
    "paths_x = ax.scatter(x[:, 0], x[:, 1], c='grey', alpha=0.5)\n",
    "circle_1 = Circle(m_1_list[:, 0], 3. * sigma_1_list[0], color='C0', alpha=0.5)\n",
    "ax.add_artist(circle_1)\n",
    "circle_2 = Circle(m_2_list[:, 0], 3. * sigma_2_list[0], color='C1', alpha=0.5)\n",
    "ax.add_artist(circle_2)\n",
    "\n",
    "# Animate.\n",
    "ani = FuncAnimation(fig, update, frames=range(no_em), interval=1000, repeat=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the optimized nuisance parameters are virtually identical to the ones used for generating the data points.\n",
    "In summary, the expectation-maximization algorithm is a competitive algorithm both in terms of speed and performance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Optimized hyperparameters and prior probabilities.\n",
    "print(m_1_list[:, -1])\n",
    "print(m_2_list[:, -1])\n",
    "print(sigma_1_list[-1])\n",
    "print(sigma_2_list[-1])\n",
    "print(pi_1_list[-1])\n",
    "print(pi_2_list[-1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Conclusion\n",
    "\n",
    "How is all this relevant to reduced-order modeling?\n",
    "It may not be obvious at first sight, but there is a one-to-one analogy between my nonparametric regression algorithm and the clustering example:\n",
    "\n",
    "1.  The model parameters are the latent variables.\n",
    "    They are continuous in reduced-order models and discrete in the clustering example.\n",
    "    But this is a detail.\n",
    "    What they have in common is that it is not possible to directly observe them.\n",
    "    In the clustering example, we use a likelihood function based on the Euclidean distance.\n",
    "    In my nonparametric regression algorithm, we use a likelihood function based on the Laplace approximation.\n",
    "2.  The hyperparameters are nuisance parameters.\n",
    "    In the clustering example, the centers and the radii are the nuisance variables.\n",
    "    In my nonparametric regression algorithm, the Gaussian process hyperparameters are the nuisance parameters.\n",
    "    They are optimized as to maximize the evidence.\n",
    "    Without proof, the responsibility-weighted averages for the centers and the radii also have the evidence-maximizing property.\n",
    "3.  The prior probabilities are also treated as hyperparameters to be optimized.\n",
    "    In the clustering example, prior probabilities are important for borderline case somewhere between the centers.\n",
    "    In my nonparametric regression algorithm, prior probabilities represent the order of magnitude of the model parameters.\n",
    "    Furthermore, they are required to compute the evidence, which in turn is required to optimize the other hyperparameters.\n",
    "    The prior probabilities are based on averaging statistics.\n",
    "    In my nonparametric regression algorithm, we combine it with a message-passing algorithm, for which there is no equivalent in the clustering example.\n",
    "\n",
    "In summary, the expectation-maximization algorithm is useful for problems involving latent variables and nuisance parameters, which often poses a chicken-or-egg problem:\n",
    "\n",
    "1.  If I do not know the nuisance parameters, I cannot estimate the likelihood of the latent variables.\n",
    "2.  If I do not know the latent variables, I cannot use them to calculate the nuisance parameters.\n",
    "\n",
    "The expectation-maximization algorithm breaks this cycle of infinite regress by starting with a random guess at the hyperparameters, and working sequentially or iteratively from there.\n",
    "Latent-variable modeling is a curious hybrid between supervised and unsupervised learning:\n",
    "On the one hand, there are observations, e.g. the coordinates of the data points.\n",
    "On the other hand, it takes quite a lot of effort, i.e. generative modeling based on nuisance parameters, to understand the structure of the data points as a whole in order to harness the available observations.\n",
    "\n",
    "Note that the expectation-maximization algorithm is an instance of the dynamic-programming principle, which for example also underlies the Viterbi algorithm and the Kalman filter.\n",
    "\n",
    "| &nbsp;              | reduced-order modeling                                 | clustering                  |\n",
    "| ------------------- | ------------------------------------------------------ | --------------------------- |\n",
    "| latent variables    | model parameters $\\theta$                              | cluster memberships/responsiblities $r_k$ |\n",
    "| nuisance parameters | Gaussian process hyperparameters $\\theta^\\mathrm{hyp}$ | cluster centers $m_k$ and radii $\\sigma_k$ |\n",
    "| prior probabilities | Gaussian process prior mean and variance               | prior probabilities $\\pi_k$ |"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
