PhD thesis tutorials
====================

Chapter 2
---------

*   Statistical inference.
    [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/hyu%2Fphd-thesis-tutorials/master?filepath=Chapter2%2FStatistical%20inference.ipynb)
*   Stochastic processes.
    [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/hyu%2Fphd-thesis-tutorials/master?filepath=Chapter2%2FStochastic%20processes.ipynb)

Chapter 3
---------

*   Least-squares regression.
    [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/hyu%2Fphd-thesis-tutorials/master?filepath=Chapter3%2FLeast-squares%20regression.ipynb)
*   Expectation-maximization algorithm.
    [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/hyu%2Fphd-thesis-tutorials/master?filepath=Chapter3%2FExpectation-maximization%20algorithm.ipynb)

Chapter 5
---------

*   Scalar advection.
    [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/hyu%2Fphd-thesis-tutorials/master?filepath=Chapter5%2FScalar%20advection.ipynb)
*   Lorenz system.
    [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/hyu%2Fphd-thesis-tutorials/master?filepath=Chapter5%2FLorenz%20system.ipynb)

Chapter 6
---------

*   Level-set methods.
    [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/hyu%2Fphd-thesis-tutorials/master?filepath=Chapter6%2FLevel-set%20methods.ipynb)
*   Eikonal field.
    [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/hyu%2Fphd-thesis-tutorials/master?filepath=Chapter6%2FEikonal%20field.ipynb)
