{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook\n",
    "\n",
    "import numpy as np\n",
    "from matplotlib.animation import FuncAnimation\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Interface kinematics\n",
    "\n",
    "Both in dynamical systems and data assimilation, there are in principle three views regarding the kinematics of an interface:\n",
    "\n",
    "> 1.  Geometric view.\n",
    "      The interface is parametrized and discretized so that one follows the motion of the whole interface by solving the laws of motion for a sufficient number of points on the interface.\n",
    "> 2.  Set-theoretic view.\n",
    "      A characteristic function is defined over the whole domain.\n",
    "      The characteristic function assumes one of two values, depending on whether the point at the location in question is inside or outside the region enclosed by the interface.\n",
    "> 3.  Analytic view.\n",
    "      A level-set function is defined over the whole domain.\n",
    "      The interface is reconstructed by identifying the position of a particular level set.\n",
    "\n",
    "[Yu et al.](https://doi.org/10.1016/j.jcp.2019.108950) discuss the consequences for data assimilation.\n",
    "In this Jupyter notebook, we illustrate that all three views give the same results when it comes to the kinematics of the interface."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Front-tracking methods\n",
    "\n",
    "We investigate the law of motion of an interface in one dimension:\n",
    "\\begin{equation}\n",
    "\\frac{\\mathrm{d} x}{\\mathrm{d} t} = 1 \\quad , \\quad\n",
    "x(0) = 0 \\quad .\n",
    "\\end{equation}\n",
    "We track the position of the interface by assigning 'markers' to points on the interface and following the positions of these markers.\n",
    "In one dimension, the interface is just a single point, and hence there is only a single marker $x$.\n",
    "We use a Euler-forward scheme for this ordinary differential equation.\n",
    "\n",
    "## Geometric view"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t_start = 0.\n",
    "t_end = 10.\n",
    "no_t = 100\n",
    "t_list = np.linspace(t_start, t_end, no_t + 1)\n",
    "dt = (t_end - t_start) / no_t\n",
    "\n",
    "def euler_forward(x):\n",
    "    return x + dt\n",
    "\n",
    "x_list = np.zeros(no_t + 1)\n",
    "for i in range(no_t):\n",
    "    x_list[i + 1] = euler_forward(x_list[i])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(1)\n",
    "ax = fig.gca()\n",
    "paths = ax.scatter(x_list[0], 0.)\n",
    "line, = ax.plot([x_list[0], x_list[0]], [-1.5, 1.5], c='k', linestyle='dashed')\n",
    "ax.set_aspect('equal')\n",
    "ax.set_title(\"iteration 0\")\n",
    "ax.set_xlabel(\"$x$\")\n",
    "ax.set_xlim([-2., 12.])\n",
    "ax.set_ylabel(\"$y$\")\n",
    "ax.set_ylim([-1.5, 1.5])\n",
    "ax.set_yticks([])\n",
    "\n",
    "def update(frame):\n",
    "    ax.set_title(\"iteration {}\".format(frame + 1))\n",
    "    paths.set_offsets([x_list[frame + 1], 0.])\n",
    "    line.set_data([x_list[frame + 1], x_list[frame + 1]], [-1.5, 1.5])\n",
    "\n",
    "ani = FuncAnimation(fig, update, frames=range(no_t), interval=100, repeat=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Front-capturing methods\n",
    "\n",
    "We rewrite the law of motion as a level-set equation:\n",
    "\\begin{equation}\n",
    "\\frac{\\partial Z}{\\partial t} + \\frac{\\partial Z}{\\partial x} = 0 \\quad ,\n",
    "\\end{equation}\n",
    "where $Z$ denotes the unspecified level-set function.\n",
    "We capture the position of the interface by advancing the values of the level-set function surronding the zero-level set in time.\n",
    "This means that the location of the zero-level set is not explicitly known until it is reconstructed from interpolating the level-set function.\n",
    "The level-set equation generalizes to higher dimensions, and also captures topological changes.\n",
    "\n",
    "We have the theory of hyperbolic partial differential equations at our disposal.\n",
    "We obviously know the analytical solution from the method of characteristics:\n",
    "\\begin{equation}\n",
    "Z(x, t) = Z(x - t, 0) \\quad .\n",
    "\\end{equation}\n",
    "For the numerical solution, we use an upwind scheme in space and a Euler-forward scheme in time for this partial differential equation.\n",
    "In particular, we use $\\mathrm{CFL} = 1$ in order to suppress numerical dissipation.\n",
    "Note that this is a numerical issue, and not related to front-capturing methods per se."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import bisect\n",
    "\n",
    "CFL = 1.\n",
    "\n",
    "x_start = t_start - 2.\n",
    "x_end = t_end + 2.\n",
    "no_x = no_t * int(x_end - x_start) // int(t_end - t_start)\n",
    "x_grid = np.linspace(x_start, x_end, no_x + 1)\n",
    "dx = dt\n",
    "\n",
    "def upwind(Z):\n",
    "    res = Z.copy()\n",
    "    res[0] = -1.\n",
    "    res[1:] = Z[1:] - CFL * (Z[1:] - Z[:-1])\n",
    "    return res"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Set-theoretic view\n",
    "\n",
    "We have the following initial condition:\n",
    "\\begin{equation}\n",
    "Z(x, t=0) = \\chi(x) = \\begin{cases}\n",
    "-1 & x < 0 \\\\\n",
    "1 & x \\geq 0\n",
    "\\end{cases} \\quad .\n",
    "\\end{equation}\n",
    "This choice of level-set function is inspired by physical phenomena such as shock tubes, premixed flames and immiscible fluids.\n",
    "Note that the values of $-1$ and $1$ are somehow arbitrary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x_sol = np.zeros((no_t + 1, no_x + 1))\n",
    "x_sol[0, :no_x * 2 // int(x_end - x_start)] = -1.\n",
    "x_sol[0, no_x * 2 // int(x_end - x_start):] = 1.\n",
    "\n",
    "for i in range(no_t):\n",
    "    x_sol[i + 1, :] = upwind(x_sol[i, :])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The set-theoretic view pertains to a front-capturing method.\n",
    "When we visualize the numerical location of the zero-level set (dashed line), we find the first index `i_loc` in the array `x_sol` representing the level-set function where `x_sol[i_loc] == 1`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(101)\n",
    "ax = fig.gca()\n",
    "i_loc = bisect.bisect_left(x_sol[0, :], 1.)\n",
    "path_1, = ax.plot(x_grid[:i_loc], x_sol[0, :i_loc], c=\"C0\")\n",
    "path_2, = ax.plot(x_grid[i_loc:], x_sol[0, i_loc:], c=\"C0\")\n",
    "line,  = ax.plot([x_grid[i_loc], x_grid[i_loc]], [-1.5, 1.5], c='k', linestyle='dashed')\n",
    "ax.plot([x_start, x_end], [0., 0.], c='k')\n",
    "ax.set_aspect('equal')\n",
    "ax.set_title(\"iteration 0\")\n",
    "ax.set_xlabel(\"$x$\")\n",
    "ax.set_xlim([-2., 12.])\n",
    "ax.set_ylabel(\"$y$\")\n",
    "ax.set_ylim([-1.5, 1.5])\n",
    "\n",
    "def update(frame):\n",
    "    ax.set_title(\"iteration {}\".format(frame + 1))\n",
    "    i_loc = bisect.bisect_left(x_sol[frame + 1, :], 1.)\n",
    "    path_1.set_data([x_grid[:i_loc], x_sol[frame + 1, :i_loc]])\n",
    "    path_2.set_data([x_grid[i_loc:], x_sol[frame + 1, i_loc:]])\n",
    "    line.set_data([x_grid[i_loc], x_grid[i_loc]], [-1.5, 1.5])\n",
    "\n",
    "ani = FuncAnimation(fig, update, frames=range(no_t), interval=100, repeat=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Analytic view\n",
    "\n",
    "We have the following initial condition:\n",
    "\\begin{equation}\n",
    "Z(x, t=0) = G(x) = \\begin{cases}\n",
    "-1 & x \\leq -1 \\\\\n",
    "x & -1 \\leq x \\leq 1 \\\\\n",
    "1 & x \\geq 1\n",
    "\\end{cases} \\quad .\n",
    "\\end{equation}\n",
    "This choice of level-set function follows Huygens' principle ($|\\nabla G| = 1$), at least inside the narrow band ($-1 \\leq x \\leq 1$)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x_sol = np.zeros((no_t + 1, no_x + 1))\n",
    "x_sol[0, :no_x * 1 // int(x_end - x_start)] = -1.\n",
    "x_sol[0, no_x * 1 // int(x_end - x_start):no_x * 3 // int(x_end - x_start)] = x_grid[no_x * 1 // int(x_end - x_start):no_x * 3 // int(x_end - x_start)]\n",
    "x_sol[0, no_x * 3 // int(x_end - x_start):] = 1.\n",
    "\n",
    "for i in range(no_t):\n",
    "    x_sol[i + 1, :] = upwind(x_sol[i, :])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The analytic view pertains to a front-capturing method.\n",
    "When we visualize the numerical location of the zero-level set (dashed line), we find the first index `i_loc` in the array `x_sol` representing the level-set function where `x_sol[i_loc] >= 0`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(111)\n",
    "ax = fig.gca()\n",
    "i_loc = bisect.bisect_left(x_sol[0, :], 0.)\n",
    "path, = ax.plot(x_grid, x_sol[0, :])\n",
    "line,  = ax.plot([x_grid[i_loc], x_grid[i_loc]], [-1.5, 1.5], c='k', linestyle='dashed')\n",
    "ax.plot([x_start, x_end], [0., 0.], c='k')\n",
    "ax.set_aspect('equal')\n",
    "ax.set_title(\"iteration 0\")\n",
    "ax.set_xlabel(\"$x$\")\n",
    "ax.set_xlim([-2., 12.])\n",
    "ax.set_ylabel(\"$y$\")\n",
    "ax.set_ylim([-1.5, 1.5])\n",
    "\n",
    "def update(frame):\n",
    "    ax.set_title(\"iteration {}\".format(frame + 1))\n",
    "    i_loc = bisect.bisect_left(x_sol[frame + 1, :], 0.)\n",
    "    path.set_data([x_grid, x_sol[frame + 1, :]])\n",
    "    line.set_data([x_grid[i_loc], x_grid[i_loc]], [-1.5, 1.5])\n",
    "\n",
    "ani = FuncAnimation(fig, update, frames=range(no_t), interval=100, repeat=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
